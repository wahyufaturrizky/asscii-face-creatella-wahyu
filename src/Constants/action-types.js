//APP
export const CHANGE_GRID = 'CHANGE_GRID'
export const LOAD_AD = 'LOAD_AD'
export const PUSH_AD = 'PUSH_AD'
export const INCREASE_MOD_COUNT = 'INCREASE_MOD_COUNT'
export const GENERATE_AD = 'GENERATE_AD'

//GRID
export const CHANGE_SORT = 'CHANGE_SORT'
export const RESET_GRID = 'RESET_GRID'
export const MOVE_IDLE_FETCH = 'MOVE_IDLE_FETCH'
export const INCREASE_PAGE_COUNT = 'INCREASE_PAGE_COUNT'
export const INCREASE_AD_INDEX = 'INCREASE_AD_INDEX'

//PRODUCT
export const ADD_PRODUCTS = 'ADD_PRODUCTS'
export const ADD_IDLE_PRODUCTS = 'ADD_IDLE_PRODUCTS'
export const LOADING_MORE = 'LOADING_MORE'
export const IDLE_FETCHING = 'IDLE_FETCHING'
export const NO_MORE_PRODUCT = 'NO_MORE_PRODUCT'
export const NO_PRODUCT_AVAILABLE = 'NO_PRODUCT_AVAILABLE'
export const FETCHING_PRODUCTS = 'FETCHING_PRODUCTS'
export const IS_ADDING_PRODUCTS = 'IS_ADDING_PRODUCTS'
