export const formatCentToDollar = number => {

  return (number / 100).toFixed(2)
}
